﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Ementexx.Dban
{
    /// <summary>
    /// Response of resolving a DBAN - 
    /// It contains the name of the account owner, the IBAN and the BIC. 
    /// Optionally it contains an amount and/or the remittance information which should be used whitin the payment data.
    /// </summary>
    [DataContract]
    public class DbanResolvingResponse
    {
        /// <summary>
        /// Name of the account owner
        /// </summary>
        [DataMember(IsRequired = true, Order = 1)]
        public string Name { get; set; }
        /// <summary>
        /// IBAN of the account
        /// </summary>
        [DataMember(IsRequired = true, Order = 2)]
        public string Iban { get; set; }
        /// <summary>
        /// BIC of the bank
        /// </summary>
        [DataMember(IsRequired = true, Order = 3)]
        public string Bic { get; set; }

        /// <summary>
        /// Amount to use in the payment
        /// </summary>
        [DataMember(IsRequired = false, Order = 4)]
        public decimal Amount { get; set; }
        /// <summary>
        /// Currency ISO code to use in the payment
        /// </summary>
        [DataMember(IsRequired = false, Order = 5)]
        [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
        public string CurrencyIsoCode { get; set; }
        /// <summary>
        /// Defines if the given amount should be editable
        /// </summary>
        [DataMember(IsRequired = false, Order = 6)]
        public bool AmountEditable { get; set; }

        /// <summary>
        /// Remittance information to use in the payment
        /// </summary>
        [DataMember(IsRequired = false, Order = 7)]
        [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
        public string RemittanceInformation { get; set; }
        /// <summary>
        /// Defines if the given remittance information should be editable
        /// </summary>
        [DataMember(IsRequired = false, Order = 8)]
        public bool RemittanceInformationEditable { get; set; }
    }
}
