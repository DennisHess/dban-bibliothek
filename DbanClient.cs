﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;

namespace Ementexx.Dban
{
    /// <summary>
    /// Provides functionality for resolving DBANs
    /// TEST - neuer Kommentar
    /// </summary>
    public static class DbanClient
    {
        /// <summary>
        /// Splits the full qualified DBAN (e.g. invoice#xycompany.de) into 
        /// the qualifier ("invoice") and the payment domain ("xycompany.de")
        /// </summary>
        /// <param name="dban">Complete DBAN</param>
        /// <param name="qualifier">Qualifier of the DBAN</param>
        /// <param name="paymentDomain">Payment domain of the DBAN</param>
        /// <exception cref="Exception">Invalid DBAN format</exception>
        public static void SplitDban(string dban, ref string qualifier, ref string paymentDomain)
        {
            string[] sl = dban.Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries);

            if (sl.Length == 2)
            {
                qualifier = sl[0];
                paymentDomain = sl[1];
            }
            else
            {
                throw new Exception("Invalid DBAN format");
            }
        }

        /// <summary>
        /// Detects the host url based on the sub-domain "payment" of the (payment) domain; 
        /// the usage of the host name is necessary, to be complient with the ssl certificate  
        /// </summary>
        /// <param name="domain">Name of a domain</param>
        /// <returns>Host url</returns>
        /// <exception cref="Exception">Host detection failed</exception>
        private static string GetHostUrl(string domain)
        {
            try
            {
                IPHostEntry hostEntry = Dns.GetHostEntry("payment." + domain);
                return string.Format("https://{0}/pdapi/", hostEntry.HostName);
            }
            catch
            {
                if (domain.ToLower() != "mydban.de")
                {
                    return GetHostUrl("mydban.de");
                }
                else
                {
                    throw new Exception("Host detection failed");
                }
            }
        }

        /// <summary>
        /// Builds the url for resolving an DBAN
        /// </summary>
        /// <param name="dban">Complete DBAN</param>
        /// <returns>Resolved url</returns>
        private static string GetDbanResolveUrl(string dban)
        {
            string qualifier = string.Empty;
            string paymentDomain = string.Empty;
            SplitDban(dban, ref qualifier, ref paymentDomain);

            return GetHostUrl(paymentDomain) + "dban/" + paymentDomain + "/" + qualifier;
        }

        /// <summary>
        /// Builds the url for listing DBAN of a payment domain
        /// </summary>
        /// <param name="paymentDomain">Payment domain</param>
        /// <returns>Resolved url</returns>
        private static string GetDbanListUrl(string paymentDomain)
        {
            return GetHostUrl(paymentDomain) + "dban/" + paymentDomain;
        }

        /// <summary>
        /// Resolves a given DBAN
        /// </summary>
        /// <param name="dban">Complete DBAN</param>
        /// <returns>Response object of type DbanResolvingResponse</returns>
        /// <exception cref="Exception">Could not resolve DBAN</exception>
        /// <example>This example shows how to call the ResolveDban method.
        /// <code lang="cs">
        /// try
        /// {
        ///     DbanResolvingResponse response = DbanClient.ResolveDban("invoice#xycompany.de"); 
        ///     Console.WriteLine("Name: " + result.Name);
        ///     Console.WriteLine("IBAN: " + result.Iban);
        ///     Console.WriteLine("BIC: " + result.Bic);
        /// }
        /// catch (Exception e)
        /// {
        ///     Console.WriteLine("Error: " + e.Message);
        /// }
        /// </code>
        /// </example>
        public static DbanResolvingResponse ResolveDban(string dban)
        {
            var client = new HttpClient();
            string url = GetDbanResolveUrl(dban);
            var response = client.GetAsync(url);

            dynamic result = response.Result.Content.ReadAsAsync<JObject>();

            if (result.Result != null)
            {
                return new DbanResolvingResponse()
                {
                    Amount = result.Result.Amount,
                    Bic = result.Result.Bic,
                    CurrencyIsoCode = result.Result.CurrencyIsoCode,
                    Iban = result.Result.Iban,
                    Name = result.Result.Name,
                    RemittanceInformation = result.Result.RemittanceInformation,
                    AmountEditable = result.Result.AmountEditable,
                    RemittanceInformationEditable = result.Result.RemittanceInformationEditable
                };
            }
            else
            {
                throw new Exception("Could not resolve DBAN");
            }
        }

        /// <summary>
        /// Lists the DBANs of a given payment domain
        /// </summary>
        /// <param name="paymentDomain">Payment domain</param>
        /// <returns>List of available DBANs</returns>
        /// <exception cref="Exception">Could not receive DBAN list</exception>
        /// <example>This example shows how to receive a DBAN list.
        /// <code lang="cs">
        /// try
        /// {
        ///     List&lt;DbanListItem&gt; dbanList = DbanClient.ListDbans("xycompany.de"); 
        ///     foreach(var dbanListItem in dbanList)
        ///     {
        ///         Console.WriteLine("DBAN: " + dbanListItem.Dban);
        ///         Console.WriteLine("Description: " + dbanListItem.Description);      
        ///     }
        /// }
        /// catch (Exception e)
        /// {
        ///     Console.WriteLine("Error: " + e.Message);
        /// }
        /// </code>
        /// </example>
        public static List<DbanListItem> ListDbans(string paymentDomain)
        {
            var client = new HttpClient();
            string url = GetDbanListUrl(paymentDomain);
            var response = client.GetAsync(url);

            dynamic result = response.Result.Content.ReadAsAsync<JArray>();

            if (result.Result != null)
            {
                List<DbanListItem> returnValue = new List<DbanListItem>();
                foreach (var item in result.Result)
                {
                    returnValue.Add(new DbanListItem() { Dban = item.Dban, Description = item.Description });
                }
                return returnValue;
            }
            else
            {
                throw new Exception("Could not receive DBAN list");
            }
        }

        /// <summary>
        /// Calculates a SHA256 hash value for the given DBAN which can be used for a manually comparison if the DBAN was typed in correctly
        /// </summary>
        /// <param name="dban">Complete DBAN</param>
        /// <param name="ignoreQualifier">If TRUE only the Payment Domain of the given DBAN will be used for
        /// calculating the hash value, the first characters of the security code will be replaced with ** **</param>
        /// <returns>DBAN security code in format XX XX - XX XX XX XX</returns>
        public static string CalculateDbanSecurityCode(string dban, bool ignoreQualifier = false)
        {
            try
            {
                string qualifier = string.Empty;
                string paymentDomain = string.Empty;
                string dbanSecurityCode = string.Empty;
                SplitDban(dban, ref qualifier, ref paymentDomain);

                if (ignoreQualifier)
                {
                    dbanSecurityCode = "** **";
                }
                else
                {
                    SHA256Managed sha256 = new SHA256Managed();
                    byte[] hashValue = sha256.ComputeHash(Encoding.UTF8.GetBytes(dban.ToLower()));
                    dbanSecurityCode = BitConverter.ToString(hashValue).Replace("-", " ").Substring(0, 5);
                }

                return string.Format("{0} - {1}", dbanSecurityCode, CalculatePaymentDomainSecurityCode(paymentDomain));
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Calculates a SHA256 hash value for the given Payment Domain
        /// </summary>
        /// <param name="paymentDomain">Payment domain</param>
        /// <returns>Payment Domain security code in format XX XX XX XX</returns>
        public static string CalculatePaymentDomainSecurityCode(string paymentDomain)
        {
            SHA256Managed sha256 = new SHA256Managed();
            byte[] hashValue = sha256.ComputeHash(Encoding.UTF8.GetBytes(paymentDomain.ToLower()));
            return BitConverter.ToString(hashValue).Replace("-", " ").Substring(0, 11);
        }

        //public static DbanContact GetDbanContact(string dban)
        //{
        //    var client = new HttpClient();
        //    string url = GetDbanContactUrl(dban);
        //    var response = client.GetAsync(url);

        //    dynamic result = response.Result.Content.ReadAsAsync<JObject>();

        //    if (result.Result != null)
        //    {
        //        return new DbanContact()
        //        {
        //            Salutation = result.Result.Salutation,
        //            FirstName = result.Result.FirstName,
        //            LastName = result.Result.LastName,
        //            CompanyName = result.Result.CompanyName,
        //            Department = result.Result.Department,
        //            JobTitle = result.Result.JobTitle,
        //            Street = result.Result.Street,
        //            AddressExtension = result.Result.AddressExtension,
        //            City = result.Result.City,
        //            PostalCode = result.Result.PostalCode,
        //            CountryCode = result.Result.CountryCode,
        //            EmailAddress = result.Result.EmailAddress,
        //            Telephone1 = result.Result.Telephone1,
        //            Telephone2 = result.Result.Telephone2,
        //            MobilePhone = result.Result.MobilePhone,
        //            Fax = result.Result.Fax
        //        };
        //    }
        //    else
        //    {
        //        throw new Exception("Could not receive DBAN contact data");
        //    }
        //}

        //private static string GetDbanContactUrl(string dban)
        //{
        //    string qualifier = string.Empty;
        //    string paymentDomain = string.Empty;
        //    SplitDban(dban, ref qualifier, ref paymentDomain);

        //    return GetHostUrl(paymentDomain) + "dbancontact/" + paymentDomain + "/" + qualifier;
        //}
    }
}
