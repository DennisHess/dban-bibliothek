﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ementexx.Dban
{
    /// <summary>
    /// Element of listing DBANs for a payment domain 
    /// </summary>
    public class DbanListItem
    {
        /// <summary>
        /// The DBAN
        /// </summary>
        public string Dban { get; set; }
        /// <summary>
        /// A public description for the given DBAN
        /// </summary>
        public string Description { get; set; }
    }
}
