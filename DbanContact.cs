﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ementexx.Dban
{
    /// <summary>
    /// Response of getting contact data related to a DBAN
    /// </summary>
    public class DbanContact
    {
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string Department { get; set; }
        public string JobTitle { get; set; }
        public string Street { get; set; }
        public string AddressExtension { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }
        public string EmailAddress { get; set; }
        public string Telephone1 { get; set; }
        public string Telephone2 { get; set; }
        public string MobilePhone { get; set; }
        public string Fax { get; set; }
    }
}
